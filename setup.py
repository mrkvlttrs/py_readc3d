from distutils.core import setup


setup(
    name='readc3d',
    version='0.0.1',
    description='Read C3D files into python and save them as something else.',
    author='[V]',
    author_email='m.vlutters@utwente.nl',
    packages=[
        'numpy',
        'pandas',
        'glog'
    ]
)