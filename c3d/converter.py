"""
Convert the flat reader data to some other format.
"""

import json
import numpy as np
import pandas as pd

import glog as log


class Converter(object):
    """
    Convert reader output to something else
    """

    def __init__(self, reader):
        self.reader = reader
        return


    def marker_unit_scale(self, point_units):
        """
        =INPUT=
            point_units - string
                Desired point units, either 'mm', 'cm', or 'm'.
        """
        
        unit_scales = {'m': 1, 'cm': 100, 'mm': 1000}
        if point_units not in unit_scales:
            return 1
        [units] = self.reader.parameter_groups['POINT'].parameters['UNITS'].data
        scale_factor = unit_scales[point_units] / unit_scales[units]
        return scale_factor


    def to_numpy(self, point_units='m'):
        """
        Put the data in numpy arrays.
        =INPUT=
            point_units - string
                See self.marker_unit_scale
        =OUTPUT=
            marker_array - ndArray
            analog_array - ndArray
        =NOTES=
            Marker data has dimensions [n_frames, n_markers, n_dimensions]
            Analog data has dimensions [n_frames, n_channels]
        """

        scale = self.marker_unit_scale(point_units)
        
        # Get marker array dimensions. The 4 is for x, y, z, residual
        n_frames = (
            self.reader.parameter_groups['POINT'].parameters['FRAMES'].data[0])
        n_markers = (
            self.reader.parameter_groups['POINT'].parameters['USED'].data[0])
        n_dimensions = 4

        # Create numpy array
        marker_array = np.array(self.reader.data_marker, dtype='float')
        marker_array.shape = (n_frames, n_markers, n_dimensions)
        marker_array[Ellipsis, 0:3] = scale * marker_array[Ellipsis, 0:3]

        # Get analog array dimensions
        n_frames = self.reader.n_analog_samples_per_frame * n_frames
        if n_frames:
            n_channels = (
                self.reader.parameter_groups['ANALOG'].parameters['USED'].data[0])

            # Create numpy array
            analog_array = np.array(self.reader.data_analog, dtype='float')
            analog_array.shape = (n_frames, n_channels)
        else:
            # Empty numpy array
            analog_array = np.array([], dtype='float')

        return marker_array, analog_array


    def save_to_numpy(self,
            marker_array, analog_array, file_name, compressed=False):
        """
        Save data to a numpy array
        
        =INPUT=
            marker_array - ndArray
            analog_array - ndArray
            file_name - string
                File name with or without path, without extension
            compressed- bool (False)
                If False, save each array to its own .npy
                If True, save both arrays to a single compressed .npz
        =NOTES=
            Pickle is not allowed by default for .npy
        """

        if compressed:
            np.savez_compressed(
                file_name, marker=marker_array, analog=analog_array)
        else:
            full_path = ''.join([file_name, '_marker'])
            np.save(full_path, marker_array, allow_pickle=False)
            full_path = ''.join([file_name, '_analog'])
            np.save(full_path, analog_array, allow_pickle=False)
        return


    def to_dict(self):
        """
        Convert the parameter group objects to dict, so it can be jsonified
        Also applies the parameter group dimension to its data, and gets
        rid of the tuple notation.
        
        =NOTES=
            All dict keys are changed to lower case.
            Data is assuemd to be a tuple.
        """

        parameter_groups = {}
        for key_g, group in self.reader.parameter_groups.items():
            # Create the parameters dictionary
            parameters = {}
            for key_p, parameter in group.parameters.items():
                key_p_lower = key_p.lower()
                parameters[key_p_lower] = {
                    key_item.lower(): data 
                    for key_item, data in vars(parameter).items()}

                # Put the data in its correct dimension
                if len(parameters[key_p_lower]['data']) == 1:
                    # String in tuple, or single number
                    data_to_pack = parameters[key_p_lower]['data'][0]
                else:
                    data_to_pack = list(parameters[key_p_lower]['data'])
                
                if len(parameters[key_p_lower]['dimensions']) > 1:
                    try:
                        packed_data = []
                        self.pack_parameter_data(
                            data_to_pack,
                            parameters[key_p_lower]['dimensions'],
                            packed_data)
                        parameters[key_p_lower]['data'] = packed_data
                    except:
                        log.warning(f'Failed to pack data for {key_p_lower}')
                else:
                    parameters[key_p_lower]['data'] = data_to_pack


            # Create the group dictionary
            parameter_group = {
                    key_item.lower(): data
                    for key_item, data in vars(group).items()}
            parameter_group['parameters'] = parameters
            parameter_groups[key_g.lower()] = parameter_group
        return parameter_groups


    def pack_parameter_data(self,
            data, dimensions, packed_data, _step=None, _reclvl=0):
        """
        Pack data in lists based on provided dimensions.

        =INPUT=
            data - iterable
                .data field from parameter group
            dimensions - tuple
                .dimensions field from parameter group.
                Assumed ordered from slowest to fastest changing dimension.
            packed_data - list
                Reference to an empty list
            _step - None or list
                Used for recursive calls only. Number of steps taken to the
                data to be distributed.
            _reclvl - int
                Used for recursive calls only. Recursion level
        =OUTPUT=
            None. Packed data is returned by reference
        =NOTES=
            WARNING: recursive function!
        """
        if _step is None:
            _step = [0]
                
        while len(packed_data) < dimensions[_reclvl]:
            if len(dimensions) == 2:
                for _ in range(0, dimensions[0]):
                    packed_data.append(
                        data[_step[0] * dimensions[1]:
                            (_step[0] + 1) * dimensions[1]])
                    _step[0] += 1

            elif len(dimensions) > 2:
                packed_data.append([])
                self.pack_parameter_data(
                    data,
                    dimensions[1::],
                    packed_data[-1],
                    _step,
                    _reclvl + 1)
        return 


    def save_to_json(self, parameter_groups, file_name):
        """
        Save parameter groups to json file.

        =INPUT=
            parameter_groups - dict
            file_name - string
                File name with or without path, without extension
        =NOTES=
            Any tuples in the data will be converted to json-compatible format.
        """
        fid = open(''.join([file_name, '.json']), 'w')
        json.dump(parameter_groups, fid)
        return


    def to_pandas(self, point_units='m'):
        """
        Save parameters and data to pandas data frame.

        TODO: nan-conversion
        """
        
        # Get the data as numpy array
        marker_array, analog_array = self.to_numpy()

        # MARKER
        # Flatten the marker array (3 to 2 dimensions)
        n_frames, n_markers, n_dimensions = marker_array.shape
        marker_array = np.reshape(marker_array,
            (n_frames, n_markers * n_dimensions))

        # Get the row indices (time)
        [n_frames] = (
            self.reader.parameter_groups['POINT'].parameters['FRAMES'].data)
        [marker_rate] = (
            self.reader.parameter_groups['POINT'].parameters['RATE'].data)
        marker_row_index = np.arange(0, n_frames) / marker_rate

        # Get the column labels
        [marker_labels] = (
            self.reader.parameter_groups['POINT'].parameters['LABELS'].data)
        dims = (self.reader.parameter_groups['POINT'].
            parameters['LABELS'].dimensions)        
        marker_column_labels = []
        for idx in range(0, dims[0]):
            idx_from = idx * dims[1]
            idx_to = dims[1] + idx * dims[1]
            marker_column_labels += [
                marker_labels[idx_from:idx_to].strip() + '_x',
                marker_labels[idx_from:idx_to].strip() + '_y',
                marker_labels[idx_from:idx_to].strip() + '_z',
                marker_labels[idx_from:idx_to].strip() + '_res']

        # Create the pandas dataframe
        dataframe_marker = pd.DataFrame(
            marker_array, index=marker_row_index, columns=marker_column_labels)

        # ANALOG
        if analog_array.size:
            # Get the row indices (time)
            [analog_rate] = (
                self.reader.parameter_groups['ANALOG'].parameters['RATE'].data)
            analog_row_index = (
                np.arange(0, int(n_frames * analog_rate / marker_rate)) /
                analog_rate)
            
            # Get the column labels
            [analog_labels] = (
                self.reader.parameter_groups['ANALOG'].parameters['LABELS'].data)
            dims = (self.reader.parameter_groups['ANALOG'].
                parameters['LABELS'].dimensions)
            analog_column_labels = [
                analog_labels[idx * dims[1]:dims[1] + idx * dims[1]].strip()
                for idx in range(0, dims[0])]

            # Create the pandas dataframes
            dataframe_analog = pd.DataFrame(analog_array,
                index=analog_row_index, columns=analog_column_labels)
        else:
            dataframe_analog = pd.DataFrame()

        return dataframe_marker, dataframe_analog


    def save_to_csv(self, dataframe_marker, dataframe_analog, file_name):
        """
        Save data to a CSV file
        
        =INPUT=
            dataframe_marker - pandas DataFrame
            dataframe_analog - pandas DataFrame
            file_name - string
                File name with or without path, without extension
        """

        full_path = ''.join([file_name, '_marker.csv'])
        dataframe_marker.to_csv(full_path)

        full_path = ''.join([file_name, '_analog.csv'])
        dataframe_analog.to_csv(full_path)

        return

    
    def save_to_hdf5(self, file_name, point_units='m'):
        "TODO: possible from pandas"
        return
