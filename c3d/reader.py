"""
See https://www.c3d.org/HTML/default.htm
"""

import struct

import glog as log


class Reader(object):
    """
    Reader object to obtain the data from a C3D file.
    Requires python 3.6 or higher due to use of formatted string literals.
    """

    def __init__(self):
        """
        Set default parameters, to be extracted from the file being read
        """

        self.fid = None

        # Header information (TODO: maybe move to header object)
        self.parameter_section_pointer = None
        self.key_value = None
        self.n_markers = 0
        self.n_analog_channels = 0
        self.n_analog_samples_per_frame = 0
        self.n_analog_measurements_per_frame = 0
        self.start_frame_raw = 0
        self.end_frame_raw = 1
        self.video_frame_rate = 0
        self.analog_frame_rate = 0
        self.max_interpolation_gap = 1
        self.scale_factor = 0
        self.is_events = False
        self.n_events = 0
        self.events = []

        # Default to intel little endian
        self.processor_type = 1
        self.endian_type = '<'

        # The actual data
        self.parameter_groups = {}
        self.data_marker = ()
        self.data_analog = ()

        return


    def read(self, file_path, header_is_truth=None):
        """
        Read data from C3D file.

        =INPUT=
            file_path - string
        =NOTES=
            Data is returned flat. Distribution over dimensions can be done
            later when converting to a desired format, such as numpy arrays.
            Reading a new file calls .__init__() again.
        """
        
        self.__init__()
        with open(file_path, 'rb') as self.fid:
            self.read_header()
            self.read_events()
            self.read_parameter_section()
            if type(header_is_truth) is bool:
                self.verify_metadata(header_is_truth)
            self.read_data()
            # self.compute_point_residuals()
        self.fid = None

        return


    def read_header(self):
        """
        Open file, find endian, read header.
        =INPUT=
            file_path - string
        """

        # From header, get endian-independent info (header word 1)
        [self.parameter_section_pointer, self.key_value] = struct.unpack(
            '!bb', self.fid.read(2))
        if int(self.key_value) != 80:
            log.error('Unable to determine processor type. Is this a c3d file?')

        # Fetch processor type and endian
        self.read_processor_type()

        # Frame info (header words 2-6)
        self.fid.seek(2 * 2 - 2)
        [self.n_markers, 
            self.n_analog_measurements_per_frame, 
            self.start_frame_raw,
            self.end_frame_raw,
            self.max_interpolation_gap] = struct.unpack(
                f'{self.endian_type}HHHHH', self.fid.read(10))

        # Scale and analog info (header words 7-9)
        [self.scale_factor] = struct.unpack(
            f'{self.endian_type}f', self.fid.read(4))
        [self.data_block_start, self.n_analog_samples_per_frame] = (
            struct.unpack(f'{self.endian_type}HH', self.fid.read(4)))
        if self.n_analog_samples_per_frame > 0:
            self.n_analog_channels = (
                self.n_analog_measurements_per_frame
                / self.n_analog_samples_per_frame)
        else:
            self.n_analog_channels = 0

        # Frame rates (header word 10)
        [self.video_frame_rate] = struct.unpack(
            f'{self.endian_type}f', self.fid.read(4))
        self.analog_frame_rate = (
            self.video_frame_rate * self.n_analog_samples_per_frame)

        # Event indicator and number of time events (header words 150 - 151)
        self.fid.seek(150 * 2 - 2)
        [event_indicator] = struct.unpack(
            f'{self.endian_type}h', self.fid.read(2))
        [self.n_events] = struct.unpack(
            f'{self.endian_type}H', self.fid.read(2))
        if event_indicator == 12345:
            self.is_events = True

        return


    def read_processor_type(self):
        """
        Get file processor type for little or big endian (parameter word 4)
        """

        self.fid.seek((self.parameter_section_pointer - 1) * 512 + 3)
        [processor_type83] = struct.unpack('!b', self.fid.read(1))
        try:
            self.processor_type = {
                1: 'intel',
                2: 'dec, vax, pdp-11',
                3: 'sgi, mips'}[processor_type83 - 83]
            self.endian_type = {1: '<', 2: '<', 3: '>'}[processor_type83 - 83]
            log.info(f"Processor type {self.processor_type}. Endian type {self.endian_type}.")
        except:
            # TODO: option to manually override.
            self.processor_type = 1
            self.endian_type = '<'
            log.warning('Unable to determine processor type. Assuming intel with little endian.')
        return


    def read_events(self):
        """
        Read events, if any. Put data in Event object.
        """
        
        if not self.is_events:
            return

        for _ in range(0, self.n_events):
            event = Event()
            self.events.append(event)

        # Event times in seconds (words 153 - 188, max 18 events)
        self.fid.seek(153 * 2 - 2)
        for event in self.events:
            event.time = struct.unpack(
                f'{self.endian_type}e', self.fid.read(2))

        # Event flags (words 189 - 197)
        self.fid.seek(189 * 2 - 2)
        for event in self.events:
            event.flag = struct.unpack(
                f'{self.endian_type}h', self.fid.read(2))

        # Event labels (words 199 - 234)
        self.fid.seek(199 * 2 - 2)
        for event in self.events:
            event.label = struct.unpack(
                f'{self.endian_type}cc', self.fid.read(2))

        return


    def read_parameter_section(self):
        """
        Read information from the parameter section
        """

        if self.parameter_section_pointer is None:
            log.error('Read the header first...')
            return

        # Parameter header (skipped at the moment. Seeking past it with + 4.)
        self.fid.seek(512 * (self.parameter_section_pointer - 1) + 4)
        # self.fid.seek(512 * (self.parameter_section_pointer - 1))
        # [_, _, n_parameter_blocks, processor_type83] = struct.unpack(
        #     f'{self.endian_type}bbbb', self.fid.read(4))

        while True:
            
            # Group / parameter id
            [n_bytes_name, group_id] = struct.unpack(
                f'{self.endian_type}bb', self.fid.read(2))

            # Determine whether group or parameter field
            if group_id < 0:
                # Group info
                offset_next = self.read_group(abs(group_id), n_bytes_name)
            elif group_id > 0:
                # Parameter info belonging to the group with the same id
                if group_id not in self.parameter_groups:
                    log.error('Encountered parameters prior to its group.')
                offset_next = self.read_parameters(group_id, n_bytes_name)
            elif group_id == 0: # n_bytes_name <= 0:
                # End reached...
                break
            else:
                log.warning('Invalid group id, stopping read_parameter_section')
                break

            # Seek to next section
            self.fid.seek(offset_next)

        # In the parameter_groups dict, swap the numbers for the names
        self.parameter_groups = {group.name: group
            for group in self.parameter_groups.values()}

        return


    def verify_metadata(self, header_is_truth=True):
        """
        Check whether our parameter info matches with that in the header
        There are some systems out there that do serious stupid stuff.

        =INPUT=
            header_is_truth - bool
                In case of a mismatch between the parameters and the header,
                if True, update the parameters based on the header
                if False, update the header based on the parameters
        =NOTES=
            Ugly code.
            Code can be simplified a bit with python3.8's walrus operator
        """
        checks = [False] * 6

        if (self.n_markers != 
                self.parameter_groups['POINT'].parameters['USED'].data[0]):
            if header_is_truth:
                self.parameter_groups['POINT'].parameters['USED'].data = (
                    self.n_markers,)
            else:
                self.n_markers = (
                    self.parameter_groups['POINT'].parameters['USED'].data[0])
            checks[0] = True

        if ('ANALOG' in self.parameter_groups and
                self.n_analog_channels !=
                self.parameter_groups['ANALOG'].parameters['USED'].data[0]):
            if header_is_truth:
                self.parameter_groups['ANALOG'].parameters['USED'].data = (
                    self.n_markers,)
            else:
                self.n_markers = (
                    self.parameter_groups['ANALOG'].parameters['USED'].data[0])
            checks[1] = True

        if (self.video_frame_rate !=
                self.parameter_groups['POINT'].parameters['RATE'].data[0]):
            if header_is_truth:
                self.parameter_groups['POINT'].parameters['RATE'].data = (
                    self.analog_frame_rate,)
            else:
                self.analog_frame_rate = (self.parameter_groups['POINT'].
                    parameters['RATE'].data[0])
            checks[2] = True

        if ('ANALOG' in self.parameter_groups and
                self.analog_frame_rate !=
                self.parameter_groups['ANALOG'].parameters['RATE'].data[0]):
            if header_is_truth:
                self.parameter_groups['ANALOG'].parameters['RATE'].data = (
                    self.analog_frame_rate,)
            else:
                self.analog_frame_rate = (self.parameter_groups['ANALOG'].
                    parameters['RATE'].data[0])
            checks[3] = True

        if ((self.end_frame_raw - self.start_frame_raw + 1) !=
                self.parameter_groups['POINT'].parameters['FRAMES'].data[0]):
            if header_is_truth:
                self.parameter_groups['POINT'].parameters['FRAMES'].data = (
                    (self.end_frame_raw - self.start_frame_raw + 1),)
            else:
                self.start_frame_raw = (-1 - self.end_frame_raw +
                    self.parameter_groups['POINT'].parameters['FRAMES'].data[0])
            checks[4] = True

        if (self.scale_factor !=
                self.parameter_groups['POINT'].parameters['SCALE'].data[0]):
            if header_is_truth:
                self.parameter_groups['POINT'].parameters['SCALE'].data = (
                    self.scale_factor,)
            else:
                self.scale_factor = (
                    self.parameter_groups['POINT'].parameters['SCALE'].data)
            checks[5] = True

        if any(checks):
            to_adjust = {True: 'parameters', False: 'header'}[header_is_truth]
            log.warning(f'Mismatch header and parameters: {checks}. Adjusted the {to_adjust}.')

        return


    def read_group(self, group_id, n_bytes_name):
        """
        =INPUT=
            group_id - int
            n_bytes_name - int
                Number of bytes of which the group or parameter name consists
        =OUTPUT=
            offset_next - int
                Offset of the next group or parameter section
        """
        # Create parameter group object
        parameter_group = ParameterGroup(group_id)

        # Fetch group data
        [group_name] = struct.unpack(
            f'{self.endian_type}{n_bytes_name}s',
            self.fid.read(n_bytes_name))
        
        # Offset of the next group or parameter
        current_offset = self.fid.tell()
        [offset_cof_next] = struct.unpack(
            f'{self.endian_type}h', self.fid.read(2))
        offset_next = offset_cof_next + current_offset

        # Description variables
        [n_bytes_group_description] = struct.unpack(
            f'{self.endian_type}b', self.fid.read(1))
        [group_description] = struct.unpack(
            f'{self.endian_type}{n_bytes_group_description}s',
            self.fid.read(n_bytes_group_description))

        # Assign data to group
        parameter_group.name = group_name.decode('utf8')
        parameter_group.description = group_description.decode('utf8')
        self.parameter_groups[group_id] = parameter_group

        return offset_next


    def read_parameters(self, group_id, n_bytes_name):
        """
        =INPUT=
            group_id - int
            n_bytes_name - int
                Number of bytes of which the group or parameter name consists
        =NOTES=
            A group with the corresponding ID must exist before calling this
            n_dimensions = 0 corresponds to a scalar
            element_length = -1 corresponds to a 1 byte character
            element_length = 1 corresponds to a 1 byte boolean
            element_length = 2 corresponds to a 2 byte integer
            element_length = 4 corresponds to a 4 byte float
        """

        # Create parameters object
        parameter_id = len(self.parameter_groups[group_id].parameters)
        parameter = Parameter(parameter_id)

        # Fetch parameter data
        [parameter_name] = struct.unpack(
            f'{self.endian_type}{n_bytes_name}s',
            self.fid.read(n_bytes_name))

        # Offset of the next group or parameter
        current_offset = self.fid.tell()
        [offset_cof_next] = struct.unpack(
            f'{self.endian_type}h', self.fid.read(2))
        offset_next = offset_cof_next + current_offset

        # Data type and number of dimensions. 
        [element_length, n_dimensions] = struct.unpack(
            f'{self.endian_type}bb', self.fid.read(2))

        # Dimension size (in elements, not in bytes)
        n_elements = 1
        if n_dimensions == 0:
            dimensions = [1]
        else:
            dimensions = []
            for _ in range(0, n_dimensions):
                dimensions.append(
                    struct.unpack(f'{self.endian_type}B', self.fid.read(1))[0])
                n_elements *= dimensions[-1]
            
            # Reverse the dimension! Fastest changing dimension is read first.
            # It's therefore put into the list first.
            dimensions.reverse()

        n_bytes = abs(element_length) * n_elements

        # Read parameter data, based on data length type
        try:
            data_type = {-1: 's', 1: '_?', 2: 'h', 4: 'f'}[element_length]
        except:
            log.error(f'Cannot handle data with elements of length {element_length}')

        # Read data (not distributed over dimensions!)
        data = struct.unpack(
                f'{self.endian_type}{n_elements}{data_type}',
                self.fid.read(n_bytes))

        # Description variables
        [n_bytes_parameter_description] = struct.unpack(
            f'{self.endian_type}b', self.fid.read(1))
        [parameter_description] = struct.unpack(
            f'{self.endian_type}{n_bytes_parameter_description}s',
            self.fid.read(n_bytes_parameter_description))

        # Assing data to parameter object
        parameter.name = parameter_name.decode('utf8')
        parameter.description = parameter_description.decode('utf8')
        parameter.dimensions = tuple(dimensions)
        if data_type == 's':
            parameter.data = (data[0].decode('utf8'),)
        else:
            parameter.data = data
        self.parameter_groups[group_id].parameters[parameter.name] = parameter

        return offset_next


    def read_data(self):
        """
        =NOTES=
            Marker and analog data are stored alternating, per frame.
            So markers frame 1, analogs frame 1, markers frame 2, etc...

            Each marker data point has 4 values: x, y, z, residual_info
            The latter requires some more conversion to obtain said data.

            According to the docs, it is not possible to store mixed data
            types within the C3D file. The POINT:SCALE parameter determines
            the type for both marker and analog data.
        """
        
        # Seek to data
        self.fid.seek((self.data_block_start - 1) * 512)

        # Determine data type based on SCALE factor
        [scale_factor] = self.get_parameter_data('POINT', 'SCALE')
        data_type, data_size = self.get_data_type(scale_factor)

        # See how much data we need to read
        n_video_frames = self.end_frame_raw - self.start_frame_raw + 1
        n_marker_measurements_per_frame = 4 * self.n_markers
        n_elements_to_read = (n_video_frames * (
            n_marker_measurements_per_frame +
            self.n_analog_measurements_per_frame))
        n_bytes_to_read = n_elements_to_read * data_size

        # Read all data
        data = struct.unpack(
            f'{self.endian_type}{n_elements_to_read}{data_type}',
            self.fid.read(n_bytes_to_read))

        # Split the data in marker and analog
        data_marker = []
        data_analog = []
        for i_frame in range(0, n_video_frames):
            
            idx_start_marker = i_frame * (n_marker_measurements_per_frame +
                self.n_analog_measurements_per_frame)
            idx_end_marker = idx_start_marker + n_marker_measurements_per_frame 
            data_marker += data[idx_start_marker:idx_end_marker]
            
            idx_start_analog = (idx_start_marker +
                n_marker_measurements_per_frame)
            idx_end_analog = (idx_start_analog +
                self.n_analog_measurements_per_frame)
            data_analog += data[idx_start_analog:idx_end_analog]

        # Scale marker data if required
        if data_type == 'h':
            # Multiply every x, y, z with the scale
             data_marker = [data_point * scale_factor if (idx + 1) % 4 != 0
                else data_point
                for idx, data_point in enumerate(data_marker)]

        # Scale analog data, if any
        if len(data_analog):
            analog_scale = self.get_parameter_data('ANALOG', 'SCALE')
            [analog_gen_scale] = self.get_parameter_data('ANALOG', 'GEN_SCALE')
            analog_offset = self.get_parameter_data('ANALOG', 'OFFSET')
            if analog_scale is not None:
                data_analog = [(data_point -
                    analog_offset[int(idx % self.n_analog_channels)]) * 
                    analog_scale[int(idx % self.n_analog_channels)] *
                    analog_gen_scale
                    for idx, data_point in enumerate(data_analog)]                

        # Store the data
        self.data_marker = tuple(data_marker)
        self.data_analog = tuple(data_analog)

        return


    def get_parameter_data(self, group_name, parameter_name):
        """
        =INPUT=
            group_name - string
                Name of one of the parameter_groups. Case insensitive.
            parameter_name - string
                Name of the parameter to fetch from the group. Case sensitive.
        =OUTPUT=
            parameter_data - 
                The desired parameter
        """
        for group in self.parameter_groups.values():
            if (group.name.lower() == group_name.lower() and
                    parameter_name in group.parameters):
                parameter_data = group.parameters[parameter_name].data
                break
        else:
            log.warning(f'Unable to find {parameter_name} in parameter group {group_name}.')
            parameter_data = None
        return parameter_data



    def get_data_type(self, scale_factor):
        """
        =INPUT=
            scale_factor - scalar
        =OUTPUT=
            data_type - string
                Either 'f' for float, or 'h' for int16
            data_size - int
                Number of bytes of the corresponding type
        """
        if scale_factor < 0:
            # Floating point format
            return 'f', 4
        elif scale_factor > 0: 
            # Signed integer format
            return 'h', 2
        log.warning(f'Invalid scale, assuming float32.')
        return 'f', 4


    def compute_point_residuals(self):
        """
        Compute cameras that contribute to the marker, and the triangulation
        residuals in the point data. The max number of cameras is 8. This
        value might be meaningless for camera arrays or setups with more than
        8 cameras.
        
        Assumes .data_marker is available and that every 4th element in it
        is a residual value.

        TODO: 
        Every fourth element in self.data_marker needs to be (re)packed as a
        16 bit signed int (h), then unpacked again as individual 8 bytes and an
        8 bit unsigned int. The latter needs to be divided by the point.scale
        for the residuals.
        """
        
        return


class Event(object):
    """
    Object for possible events
    """

    def __init__(self):
        self.label = ''
        self.flag = 0
        self.time = 0
        return


class ParameterGroup(object):
    """
    Object for parameter group holding parameters
    """

    def __init__(self, id):
        self.id = id
        self.name = ''
        self.description = ''
        self.parameters = {}
        return


    def to_json(self):
        # TODO
        return


class Parameter(object):
    """
    Object for a parameter
    """

    def __init__(self, id):
        self.id = id
        self.name = ''
        self.description = ''
        self.dimensions = ()
        self.data = None
        return
