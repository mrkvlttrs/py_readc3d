from .reader import Reader 
from .converter import Converter


class BasicData(object):

    def __init__(self, parameter_groups=None, marker=None, analog=None):
        self.parameter_groups = parameter_groups
        self.marker = marker
        self.analog = analog
        return


class BasicReader(object):
    """
    Wrapper to standardize C3D reader output.
    See self.read docstring.
    """

    def __init__(self):
        self.reader = Reader()
        self.converter = Converter(self.reader)
        return


    def read(self, file_path):
        """
        =INPUT=
            file_path - string
        =OUTPUT=
            parameter_groups - dict
            marker - ndArray
            analog - ndArray
        =NOTES=
            Removes spaces from the marker label names
            TODO: possibly also remove them from analog label names
        """
        self.reader.read(file_path, header_is_truth=True)
        marker, analog = self.converter.to_numpy(point_units='m')
        parameter_groups = self.converter.to_dict()
        
        # Remove spaces from the marker label names
        label_list = parameter_groups["point"]["parameters"]["labels"]["data"]
        parameter_groups["point"]["parameters"]["labels"]["data"] = [
            label.replace(" ", "") for label in label_list]

        return BasicData(parameter_groups, marker, analog)