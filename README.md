
## Usage
See sample/testfile.py
See c3d/wrapper.py

## Format
When flat (1D), the data order for marker data is
* per frame
* per marker
* per data point: x, y, z, residual, 

When flat (1D), the data order for analog data is
* per frame
* per channel