"""
Test
Assumed to be run from the repository root
"""

import c3d
from matplotlib import pyplot as plt


file_path = "./sample/trial0103.c3d" 

reader = c3d.Reader()
converter = c3d.Converter(reader)

reader.read(file_path, header_is_truth=True)
pg = converter.to_dict()
marker, analog = converter.to_numpy(point_units='m')
# df_marker, df_analog = converter.to_pandas(point_units='m')

# converter.save_to_json(pg, '/home/mark/Documents/git/test')
# converter.save_to_numpy(marker, analog, '/home/mark/Documents/git/test')
# converter.save_to_csv(df_marker, df_analog, '/home/mark/Documents/git/test')

# converter.save_to_numpy(file_name='/home/mark/Documents/git/test', point_units='m')
# converter.save_to_json(file_name='/home/mark/Documents/git/test')
# converter.save_to_csv(file_name='/home/mark/Documents/git/test', point_units='m')

fig = plt.figure()
ax1 = plt.subplot(3, 1, 1)
ax2 = plt.subplot(3, 1, 2)
ax3 = plt.subplot(3, 1, 3)

ax1.plot(marker[:, 0, 0])
ax2.plot(marker[:, 0, 1])
ax3.plot(marker[:, 0, 2])

plt.show()
